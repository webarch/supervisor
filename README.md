# Ansible Debian Supervisor Role 

This repository contains an Ansible role for installing [Supervisor](http://supervisord.org/) on Debian servers. 

The only change from the default configuration is to add:

```
user = root
```

To the `supervisord.conf` file, see [this issue](https://github.com/Supervisor/supervisor/issues/308).

To use this role you need to use Ansible Galaxy to install it into another repository under `galaxy/roles/supervisor` by adding a `requirements.yml` file in that repo that contains:

```yml
---
- name: supervisor
  src: https://git.coop/webarch/supervisor.git
  version: master
  scm: git
```

And a `ansible.cfg` that contains:

```
[defaults]
retry_files_enabled = False
pipelining = True
inventory = hosts.yml
roles_path = galaxy/roles

```

And a `.gitignore` containing:

```
roles/galaxy
```

To pull this repo in run:

```bash
ansible-galaxy install -r requirements.yml --force 
```

The other repo should also contain a `supervisor.yml` file that contains:

```yml
---
- name: Install Supervisor
  become: yes

  hosts:
    - supervisor_servers

  roles:
    - supervisor
```

And a `hosts.yml` file that contains lists of servers, for example:

```yml
---
all:
  children:
    supervisor_servers:
      hosts:
        host3.example.org:
        host4.example.org:
        cloud.example.com:
        cloud.example.org:
        cloud.example.net:
```

Then it can be run as follows:

```bash
ansible-playbook supervisor.yml 
```
